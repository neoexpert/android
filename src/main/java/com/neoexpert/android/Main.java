package com.neoexpert.android;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.io.*;
import java.nio.*;

public class Main{
	private static ByteBuffer buf;
	private static int string_ids_off;
	public static void main(String[] args) throws IOException{
		ZipFile apk=new ZipFile(args[0]);
		ZipEntry classes=apk.getEntry("classes.dex");
		InputStream raw = apk.getInputStream(classes);
		ByteArrayOutputStream buffer = new ByteArrayOutputStream((int)classes.getSize());

		int nRead;
		byte[] bytes = new byte[(int)classes.getSize()];

		while ((nRead = raw.read(bytes, 0, bytes.length)) != -1) {
			buffer.write(bytes, 0, nRead);
		}

		bytes = buffer.toByteArray();

		buf = ByteBuffer.wrap(bytes);

		long magic=buf.getLong();
		System.out.print("magic number: ");
		System.out.println(Long.toHexString(magic));
		int checksum=buf.getInt();
		byte[] signature=new byte[20];
		buf.get(signature);
		int file_size=buf.getInt();

		System.out.print("file size: ");
		System.out.println(little2big(file_size));

		int header_size=buf.getInt();

		System.out.print("header size: ");
		System.out.println(little2big(header_size));


		int endian_tag=buf.getInt();

		System.out.print("endian_tag: ");
		System.out.println(little2big(endian_tag));


		int link_size=buf.getInt();

		System.out.print("link_size: ");
		System.out.println(little2big(link_size));

		int link_off=buf.getInt();

		System.out.print("link_off: ");
		System.out.println(little2big(link_off));

		int map_off=buf.getInt();

		System.out.print("map_off: ");
		System.out.println(little2big(map_off));

		int string_ids_size=buf.getInt();

		System.out.print("string_ids_size: ");
		System.out.println(little2big(string_ids_size));

		string_ids_off=little2big(buf.getInt());

		System.out.print("string_ids_off: ");
		System.out.println(little2big(string_ids_off));


		int type_ids_size=buf.getInt();

		System.out.print("type_ids_size: ");
		System.out.println(little2big(type_ids_size));

		int type_ids_off=buf.getInt();

		System.out.print("type_ids_off: ");
		System.out.println(little2big(type_ids_off));


		int proto_ids_size=buf.getInt();

		System.out.print("proto_ids_size: ");
		System.out.println(little2big(proto_ids_size));

		int proto_ids_off=buf.getInt();

		System.out.print("proto_ids_off: ");
		System.out.println(little2big(proto_ids_off));


		int field_ids_size=buf.getInt();

		System.out.print("field_ids_size: ");
		System.out.println(little2big(field_ids_size));

		int field_ids_off=buf.getInt();

		System.out.print("field_ids_off: ");
		System.out.println(little2big(field_ids_off));


		int method_ids_size=buf.getInt();

		System.out.print("method_ids_size: ");
		System.out.println(little2big(method_ids_size));

		int method_ids_off=buf.getInt();

		System.out.print("method_ids_off: ");
		System.out.println(little2big(method_ids_off));



		int class_defs_size=little2big(buf.getInt());

		System.out.print("class_defs_size: ");
		System.out.println(class_defs_size);

		int class_defs_off=buf.getInt();

		System.out.print("class_defs_off: ");
		System.out.println(little2big(class_defs_off));



		int data_size=buf.getInt();

		System.out.print("data_size: ");
		System.out.println(little2big(data_size));

		int data_off=buf.getInt();

		System.out.print("data_off: ");
		System.out.println(little2big(data_off));

		int soff=buf.getInt();
		System.out.print("soff: ");
		System.out.println(little2big(soff));

		//CLASSES
		buf.position(little2big(class_defs_off));
		for(int i=0;i<class_defs_size;i++){
				int class_idx=buf.getInt();
				int access_flags=buf.getInt();
				int superclass_idx=buf.getInt();
				int interfaces_off=buf.getInt();
				int source_file_idx=buf.getInt();
				int position=buf.position();
				System.out.println(readString(little2big(source_file_idx)));
				buf.position(position);
				int annotations_off=buf.getInt();
				int class_data_off=buf.getInt();
				int static_values_off=buf.getInt();
		}
	}

	public static String readString(int id)throws IOException{
			buf.position(string_ids_off+id*4);
			int first_string_pos=buf.getInt();
			buf.position(little2big(first_string_pos));
			int length=(int)decodeUleb128(buf);
			char[] out=new char[length];
			String s= decodeUTF16(buf,out);
			return s;
	}

	public static String decodeUTF16(ByteBuffer in, char[] out) throws UTFDataFormatException {
        int s = 0;
        while (true) {
            char a = (char) (in.get() & 0xff);
            if (a == 0) {
                return new String(out, 0, s);
            }
            out[s] = a;
            if (a < '\u0080') {
                s++;
            } else if ((a & 0xe0) == 0xc0) {
                int b = in.get() & 0xff;
                if ((b & 0xC0) != 0x80) {
                    throw new UTFDataFormatException("bad second byte");
                }
                out[s++] = (char) (((a & 0x1F) << 6) | (b & 0x3F));
            } else if ((a & 0xf0) == 0xe0) {
                int b = in.get() & 0xff;
                int c = in.get() & 0xff;
                if (((b & 0xC0) != 0x80) || ((c & 0xC0) != 0x80)) {
                    throw new UTFDataFormatException("bad second or third byte");
                }
                out[s++] = (char) (((a & 0x0F) << 12) | ((b & 0x3F) << 6) | (c & 0x3F));
            } else {
                throw new UTFDataFormatException("bad byte");
            }
        }
    }

	private final static int BITS_LONG = 64;
    private final static int MASK_DATA = 0x7f;
    private final static int MASK_CONTINUE = 0x80;
	private static long decodeUleb128(ByteBuffer bytes) throws IOException {
        long value = 0;
        int bitSize = 0;
        int read;

        do {
            if ((read = bytes.get()) == -1) {
                throw new IOException("Unexpected EOF");
            }

            value += ((long) read & MASK_DATA) << bitSize;
            bitSize += 7;
            if (bitSize >= BITS_LONG) {
                throw new ArithmeticException("ULEB128 value exceeds maximum value for long type.");
            }

        } while ((read & MASK_CONTINUE) != 0);
        return value;
    }

	static int little2big(int i) {
    	return (i&0xff)<<24 | (i&0xff00)<<8 | (i&0xff0000)>>8 | (i>>24)&0xff;
	}
}
